CFLAGS	+= -W -Wall -pedantic -O3
CFLAGS	+= -I./header/

CC	= gcc
RM	= rm -f

LIB	= -lm

NAME	= myz

SRC	+= \
	src/main.c \
	src/my_string.c \
	src/file.c \
	src/gen.c \
	src/myz.c \
	src/myz_crypt.c \
	src/bit.c \
	src/print.c \
	src/rw_header.c \
	src/compress.c \
	src/uncompress.c

OBJ	= $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(CFLAGS) $(LIB) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
