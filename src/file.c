#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "const.h"
#include "my_string.h"

int		my_open(const char *filename, const int mode)
{
  struct stat	s;
  int		file;
  int		fd;

  file = 0;
  if (mode & O_CREAT)
    {
      if (access(filename, F_OK) != -1)
	{
	  if (stat(filename, &s) == -1)
	    {
	      fprintf(stderr, "%s: \"%s\": no such file or directory\n", PROG_NAME, filename);
	      return (-1);
	    }
	  file = 1;
	}
    }
  else
    {
      if (access(filename, F_OK) == -1 || stat(filename, &s) == -1)
	{
	  fprintf(stderr, "%s: \"%s\": no such file or directory\n", PROG_NAME, filename);
	  return (-1);
	}
      file = 1;
    }
  if (file && !(s.st_mode & S_IFREG))
    {
      fprintf(stderr, "%s: \"%s\" is not a file\n", PROG_NAME, filename);
      return (-1);
    }
  if ((fd = open(filename, mode, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH)) == -1)
    fprintf(stderr, "%s: \"%s\": no such file or directory\n", PROG_NAME, filename);
  return (fd);
}

int	my_read(const int fd, char *str, const int size)
{
  int	ret;

  if ((ret = read(fd, str, size)) == -1)
    return (-1);
  str[ret] = '\0';
  return (ret);
}

char	*get_file(const char *filename, uint *filesize)
{
  char	*file;
  char	*buff;
  char	*tmp;
  int	fd;
  int	size;
  int	i;

  file = NULL;
  if (filesize)
    *filesize = 0;
  if ((buff = malloc(sizeof(*buff) * (BLOCK_SIZE + 1))) == NULL)
    return (NULL);
  if ((fd = my_open(filename, O_RDONLY)) == -1)
    return (NULL);
  i = 0;
  while ((size = my_read(fd, buff, BLOCK_SIZE)) > 0)
    {
      if (filesize)
	*filesize += size;
      tmp = file;
      if ((file = malloc(sizeof(*file) * ((BLOCK_SIZE * i) + (size + 1)))) == NULL)
	return (NULL);
      if (memset(file, 0, (BLOCK_SIZE * i) + (size + 1)) == NULL)
	return (NULL);
      if (tmp)
	{
	  if (my_strncpy(file, tmp, BLOCK_SIZE * i) == NULL)
	    return (NULL);
	  free(tmp);
	}
      if (my_strncpy(&file[BLOCK_SIZE * i], buff, size) == NULL)
	return (NULL);
      ++i;
    }
  close(fd);
  free(buff);
  return (file);
}
