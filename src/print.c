#include <unistd.h>
#include <stdio.h>

#include "compress.h"
#include "const.h"

void	print_help(void)
{
  printf("%s: help\n", PROG_NAME);
  printf("USAGE: %s [OPTION]... [FILE...]\n", PROG_NAME);
  printf("OPTIONS:\n");
  printf("  -c                    Compress following files\n");
  printf("  -u                    Uncompress following files\n");
  printf("  --help                Will print this help\n");
  printf("  --name [filename]     Set new file name to [filename]\n");
  printf("  --key [key]           Crypt following files with the key [key]\n");
  printf("  --bit_len [len]       Define compressed bit length on following files (4 by default)\n");
  printf("\n");
  printf("Exit Status:\n");
  printf("  0    It is OK\n");
  printf("  1    It failled\n");
}

void		print_cent(float cent)
{
  static int	tmp = 100;
  int		nb;
  int		i;

  if (tmp != (int)cent)
    {
      tmp = (int)cent;
      fprintf(stdout, "\r[");
      nb = (20 * cent) / 100;
      i = 0;
      while (i < nb)
	{
	  fprintf(stdout, "|");
	  ++i;
	}
      while (i < 20)
	{
	  fprintf(stdout, " ");
	  ++i;
	}
      fprintf(stdout, "] %.0f%%", cent);
      fflush(stdout);
    }
}
