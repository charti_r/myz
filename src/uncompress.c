#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>

#include "compress.h"
#include "const.h"
#include "my_string.h"
#include "file.h"

static void	algo(const char *block, char *unblock, const t_block_hd *hd, const int bit_len)
{
  uint		i;

  i = 0;
  while (i < hd->size.value)
    {
      if (get_nextbit(block, 0))
	unblock[i] = unz_comp(block, hd, bit_len, i == (hd->size.value - 1));
      else
	unblock[i] = unz_uncomp(block, i == (hd->size.value - 1));
      ++i;
    }
}

static int	uncompress_block(const int fd, char *block, const t_block_hd *hd, const int bit_len, const char *key)
{
  char		*unblock;

  if ((unblock = malloc(sizeof(*unblock) * hd->size.value)) == NULL)
    return (-1);

  algo(block, unblock, hd, bit_len);
  if (key && myz_uncrypt(unblock, hd->size.value, key) == -1)
    return (-1);

  write(fd, unblock, hd->size.value);
  free(unblock);
  return (0);
}

int		uncompress(char *filename, char *new_filename, const char *key)
{
  t_file_hd	file_hd;
  t_block_hd	block_hd;
  char		*block;
  int		size;
  int		fd[2];
  uint		i_block;

  if ((fd[0] = my_open(filename, O_RDONLY)) == -1)
    return (1);
  if ((read_file_header(fd[0], &file_hd)) == -1)  /* read file header */
    return (-1);

  if (new_filename == NULL)
    new_filename = file_hd.filename;
  if ((fd[1] = my_open(new_filename, O_WRONLY | O_CREAT | O_TRUNC)) == -1)
    return (-1);
  free(file_hd.filename);

  i_block = 0;
  while (i_block < file_hd.nb_block.value)
    {
      if ((read_block_header(fd[0], &block_hd)) == -1) /* read block header */
	return (-1);
      if ((block = malloc(sizeof(*block) * block_hd.nb_wr.value)) == NULL)
	return (-1);
      if ((size = read(fd[0], block, block_hd.nb_wr.value)) == -1)
	return (-1);

      uncompress_block(fd[1], block, &block_hd, file_hd.bit_len.value, key);
      free(block);
      free(block_hd.top);
      ++i_block;
    }
  close(fd[0]);
  close(fd[1]);
  return (0);
}
