#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "compress.h"

static void	gen_table(const char *file, const uint size, uint table[256])
{
  uint		i;

  i = 0;
  while (i < 256)
    {
      table[i] = 0;
      ++i;
    }
  i = 0;
  while (i < size)
    {
      ++table[(uchar)file[i]];
      ++i;
    }
}

char	*gen_top(const char *file, const uint size, const int top_size)
{
  uint	table[256];
  char	*top;
  uchar	tmp;
  int	set, add;
  int	i, j, k;

  gen_table(file, size, table);
  if ((top = malloc(sizeof(*top) * top_size)) == NULL)
    return (NULL);
  i = -1;
  while (++i < top_size)
    top[i] = 0;
  i = -1;
  while (++i < top_size)
    {
      set = 0;
      tmp = 0;
      j = -1;
      while (++j < 256)
	if (!set || table[j] > table[tmp])
	  {
	    add = 1;
	    k = -1;
	    while (add && ++k < i)
	      add = ((uchar)top[k] == (uchar)(j)) ? 0 : add;
	    tmp = add ? j : tmp;
	    set = add ? 1 : set;
	  }
      top[i] = tmp;
    }
  return (top);
}
