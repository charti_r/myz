#include <unistd.h>
#include <stdlib.h>

#include "file.h"
#include "compress.h"

void	write_file_header(const int fd, const t_file_hd *hd)
{
  write(fd, hd->filename_size.octet, 4);
  write(fd, hd->filename, hd->filename_size.value);
  write(fd, hd->nb_block.octet, 4);
  write(fd, hd->bit_len.octet, 4);
}

void	write_block_header(const int fd, const t_block_hd *hd)
{
  write(fd, hd->size.octet, 4);
  write(fd, hd->nb_wr.octet, 4);
  write(fd, hd->top_size.octet, 4);
  write(fd, hd->top, hd->top_size.value);
}

int	read_file_header(const int fd, t_file_hd *hd)
{
  if ((read(fd, hd->filename_size.octet, 4)) == -1)
    return (-1);

  if ((hd->filename = malloc(sizeof(*hd->filename) * (hd->filename_size.value + 1))) == NULL)
    return (-1);
  if ((my_read(fd, hd->filename, hd->filename_size.value)) == -1)
    return (-1);

  if ((read(fd, hd->nb_block.octet, 4)) == -1)
    return (-1);

  if ((read(fd, hd->bit_len.octet, 4)) == -1)
    return (-1);
  return (0);
}

int	read_block_header(const int fd, t_block_hd *hd)
{
  if ((read(fd, hd->size.octet, 4)) == -1)
    return (-1);

  if ((read(fd, hd->nb_wr.octet, 4)) == -1)
    return (-1);

  if ((read(fd, hd->top_size.octet, 4)) == -1)
    return (-1);

  if ((hd->top = malloc(sizeof(*hd->top) * (hd->top_size.value + 1))) == NULL)
    return (-1);
  if ((my_read(fd, hd->top, hd->top_size.value)) == -1)
    return (-1);
  return (0);
}
