#include <unistd.h>

#include "compress.h"

int		add_bit(char *str, const int value, const int last)
{
  static uchar	octet = 0;
  static int	bit = 0;
  static int	i = 0;
  int		ret = 0;

  octet <<= 1;
  octet |= value ? 1 : 0;
  bit = (bit + 1) % 8;
  if (!bit)
    {
      str[i] = octet;
      ret = 1;
      ++i;
    }
  else if (last)
    {
      str[i] = octet << (8 - bit);
      ret = 1;
      octet = 0;
      bit = 0;
      i = 0;
    }
  return (ret);
}

int		get_nextbit(const char *str, const int last)
{
  static int	bit = 0;
  static int	i = 0;
  int		ret;

  ret = (str[i] >> (7 - bit)) & 1;
  bit = (bit + 1) % 8;
  if (!bit)
    ++i;
  if (last)
    {
      bit = 0;
      i = 0;
    }
  return (ret);
}
