#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "compress.h"
#include "my_string.h"
#include "const.h"

static int	running(char **argv)
{
  int		ret = 0;
  char		*filename = NULL;
  char		*key = NULL;
  char		option = 'c';
  int		bit_len = BIT_LEN;
  int		i;

  i = 0;
  while (argv[++i])
    {
      if (strcmp(argv[i], "--help") == 0)
	print_help();
      else if (strcmp(argv[i], "--bit_len") == 0)
	{
	  ++i;
	  if (argv[i] && my_str_isnum(argv[i]))
	    bit_len = atoi(argv[i]);
	  else
	    {
	      fprintf(stderr, "%s: [--bit_len] option expect an integer number\n", PROG_NAME);
	      return (-1);
	    }
	}
      else if (strcmp(argv[i], "--key") == 0)
	{
	  if (argv[++i])
	    key = argv[i];
	  else
	    {
	      fprintf(stderr, "%s: [--key] option expect a character string\n", PROG_NAME);
	      return (-1);
	    }
	}
      else if (strcmp(argv[i], "--name") == 0)
	{
	  if (argv[++i])
	    filename = argv[i];
	  else
	    {
	      fprintf(stderr, "%s: [--name] option expect a character string\n", PROG_NAME);
	      return (-1);
	    }
	}
      else if (strcmp(argv[i], "-c") == 0)
	option = 'c';
      else if (strcmp(argv[i], "-u") == 0)
	option = 'u';
      else
	{
	  printf("%s\n", argv[i]);
	  if (option == 'c')
	    {
	      ret = compress(argv[i], filename, bit_len, key) == 1 ? 1 : ret;
	      if (ret == -1)
		return (-1);
	    }
	  else if (option == 'u')
	    {
	      ret = uncompress(argv[i], filename, key) == 1 ? 1 : ret;
	      if (ret == -1)
		return (-1);
	    }
	  filename = NULL;
	}
    }
  return (ret);
}

int	main(int argc, char **argv)
{
  int   ret;

  if (argc < 2)
    {
      print_help();
      return (1);
    }
  ret = running(argv);
  return (ret == -1 ? 1 : ret);
}
