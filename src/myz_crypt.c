#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

#include "compress.h"
#include "file.h"

void	myz_crypt(char *str, const int size, const char *key)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (i < size)
    {
      str[i] = (str[i] + key[j]) - j;
      j = key[j + 1] ? (j + 1) : 0;
      ++i;
    }
}

int	myz_uncrypt(char *str, const int size, const char *key)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (i < size)
    {
      str[i] = (str[i] + j) - key[j];
      j = key[j + 1] ? (j + 1) : 0;
      ++i;
    }
  return (0);
}
