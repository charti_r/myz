#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "compress.h"
#include "const.h"
#include "my_string.h"
#include "file.h"

static int	get_topchar(const char *top, const int top_size, const char c)
{
  int		i;

  i = -1;
  while (++i < top_size)
    if (top[i] == c)
      return (i);
  return (-1);
}

static int	algo(const int fd, const char *block, const int bit_len, t_block_hd *hd)
{
  short		index;
  char		*buff;
  int		nb_wr;
  uint		i;

  if ((buff = malloc(sizeof(*buff) * (BLOCK_SIZE + (BLOCK_SIZE << 3)))) == NULL)
    return (-1);
  memset(buff, 0, BLOCK_SIZE + (BLOCK_SIZE << 3));

  i = 0;
  nb_wr = 0;
  while (i < hd->size.value)
    {
      if ((index = get_topchar(hd->top, hd->top_size.value, block[i])) != -1)
	nb_wr += myz_comp(buff, index, bit_len, (i == hd->size.value - 1) ? 1 : 0);
      else
	nb_wr += myz_uncomp(buff, block[i], (i == hd->size.value - 1) ? 1 : 0);
      ++i;
    }
  hd->nb_wr.value = nb_wr;
  write_block_header(fd, hd); /* write block header */
  write(fd, buff, nb_wr);

  printf("free start\n");
  free(buff);
  printf("free done\n");
  return (0);
}

static int	compress_block(const int fd, char *block, const int size, const int bit_len, const char *key)
{
  t_block_hd	block_hd;

  if (key)
    myz_crypt(block, size, key);

  block_hd.size.value = size;
  block_hd.top_size.value = pow(2, bit_len);
  if ((block_hd.top = gen_top(block, block_hd.size.value, block_hd.top_size.value)) == NULL)
    return (-1);

  if ((algo(fd, block, bit_len, &block_hd)) == -1)
    return (-1);
  free(block_hd.top);
  return (0);
}

int		compress(char *filename, char *new_filename, const int bit_len, const char *key)
{
  struct stat	s;
  t_file_hd	file_hd;
  int		ndef_name;
  char		*block;
  int		size;
  int		fd[2];
  int		i_block;

  ndef_name = (new_filename == NULL);
  if (ndef_name && (new_filename = my_addstr(filename, EXT)) == NULL)
    return (-1);

  if ((fd[0] = my_open(filename, O_RDONLY)) == -1)
    return (1);
  if ((fd[1] = my_open(new_filename, O_WRONLY | O_CREAT | O_TRUNC)) == -1)
    return (-1);

  if (ndef_name)
    free(new_filename);

  if (stat(filename, &s) == -1)
    return (-1);
  file_hd.filename_size.value = strlen(filename);
  file_hd.filename = filename;
  file_hd.nb_block.value = (s.st_size / BLOCK_SIZE) + (s.st_size % BLOCK_SIZE ? 1 : 0);
  file_hd.bit_len.value = bit_len;
  write_file_header(fd[1], &file_hd); /* write file header */

  if ((block = malloc(sizeof(*block) * BLOCK_SIZE)) == NULL)
    return (-1);

  i_block = 0;
  while ((size = read(fd[0], block, BLOCK_SIZE)) > 0)
    {
      if ((compress_block(fd[1], block, size, bit_len, key)) == -1)
	return (-1);
      ++i_block;
      printf("block: %d\n", i_block);
    }
  free(block);
  close(fd[0]);
  close(fd[1]);
  return (0);
}
