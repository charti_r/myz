#include <stdlib.h>
#include <string.h>

char	*my_addstr(const char *s1, const char *s2)
{
  char	*new_str;

  if ((new_str = malloc(sizeof(*new_str) * (strlen(s1) + strlen(s2) + 1))) == NULL)
    return (NULL);
  if (strcpy(new_str, s1) == NULL)
    return (NULL);
  return (strcat(new_str, s2));
}

char	*my_strncpy(char *dest, const char *src, const int n)
{
  int	i;

  i = -1;
  while (++i < n)
    dest[i] = src[i];
  return (dest);
}

int	my_str_isnum(const char *str)
{
  int	i;

  i = -1;
  while (str[++i])
    if (str[i] < '0' || str[i] > '9')
      return (0);
  return (1);
}
