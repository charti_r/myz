#include <unistd.h>

#include "compress.h"

int	myz_comp(char *str, const int index, const int bit_len, const int last)
{
  int	ret;
  int	i;

  ret = 0;
  ret += add_bit(str, BTYPE_C, 0);
  i = bit_len;
  while (--i >= 0)
    ret += add_bit(str, (index >> i) & 1, i ? 0 : last);
  return (ret);
}

int	myz_uncomp(char *str, const uchar c, const int last)
{
  int	ret;
  int	i;

  ret = 0;
  ret += add_bit(str, BTYPE_UC, 0);
  i = 8;
  while (--i >= 0)
    ret += add_bit(str, (c >> i) & 1, i ? 0 : last);
  return (ret);
}

uchar	unz_comp(const char *block, const t_block_hd *hd, const int bit_len, const int last)
{
  uchar	octet;
  int	i;

  octet = 0;
  i = -1;
  while (++i < bit_len)
    {
      octet <<= 1;
      octet |= (get_nextbit(block, (i == (bit_len - 1)) ? last : 0) ? 1 : 0);
    }
  return (hd->top[octet]);
}

uchar	unz_uncomp(const char *block, const int last)
{
  uchar	octet;
  int	i;

  octet = 0;
  i = -1;
  while (++i < 8)
    {
      octet <<= 1;
      octet |= (get_nextbit(block, (i == 7) ? last : 0) ? 1 : 0);
    }
  return (octet);
}
