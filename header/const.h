#ifndef CONST_H_
# define CONST_H_

# include <limits.h>

# define BLOCK_SIZE	(4096)
# define PROG_NAME	"myz"
# define EXT		".myz"

# define BIT_LEN	4
# define LN2		0.693147181
# define BTYPE_C	1
# define BTYPE_UC	0

#endif /* !CONST_H_ */
