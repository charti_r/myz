#ifndef MY_STRING_H
# define MY_STRING_H

char	*my_addstr(const char *s1, const char *s2);
char	*my_strncpy(char *dest, const char *src, const int n);
int	my_str_isnum(const char *str);

#endif /* !MY_STRING_H */
