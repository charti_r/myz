#ifndef COMPRESS_H_
# define COMPRESS_H_

# include <sys/types.h>

# include "const.h"

typedef unsigned char	uchar;
typedef union		data
{
  uint			value;
  char			octet[4];
}			u_data;

typedef struct		s_file_hd
{
  u_data		filename_size;
  char			*filename;
  u_data		nb_block;
  u_data		bit_len;
}			t_file_hd;

typedef struct		s_block_hd
{
  u_data		size;
  u_data		nb_wr;
  u_data		top_size;
  char			*top;
}			t_block_hd;

int	compress(char *filename, char *new_filename, const int bit_len, const char *key);
int	uncompress(char *filename, char *new_filename, const char *key);

char	*gen_top(const char *file, const uint size, const int top_size);
int	myz_comp(char *str, const int index, const int bit_len, const int last);
int	myz_uncomp(char *str, const uchar c, const int last);
uchar	unz_comp(const char *block, const t_block_hd *hd, const int bit_len, const int last);
uchar	unz_uncomp(const char *block, const int last);

void	myz_crypt(char *str, const int size, const char *key);
int	myz_uncrypt(char *str, const int size, const char *key);

int	add_bit(char *str, const int value, const int last);
int	get_nextbit(const char *str, const int last);

void	print_help(void);
void	print_cent(float cent);

#endif /* !COMPRESS_H_ */
