#ifndef FILE_H_
# define FILE_H_

# include <sys/types.h>

# include "compress.h"

int	my_open(const char *filename, const int mode);
int	my_read(const int fd, char *str, const int size);
char	*get_file(const char *filename, uint *filesize);

void	write_file_header(const int fd, const t_file_hd *hd);
void	write_block_header(const int fd, const t_block_hd *hd);
int	read_file_header(const int fd, t_file_hd *hd);
int	read_block_header(const int fd, t_block_hd *hd);

#endif /* !FILE_H_ */
